$(document).ready(function () {

//Tabs

    var tabs = $('#tabs');

    tabHeightInit();

    if ($(window).width() < 961) {
        mobileTabInit();
    }

    $(window).on('resize', function () {
        tabHeightInit();
        if ($(window).width() < 961) {
            mobileTabInit();
        } else {
            tabHeightInit();
        }
    });

    $('.tab-item').on('click', function () {
        var tabItem = $('.tab-item');
        tabHeightInit();

        if ($(window).width() > 960) {
            tabItem.removeClass('active');
            $(this).addClass('active');
            tabHeightInit();
        } else {
            var tabContHeight = $(this).find('.tab-content').height();
            if (!$(this).hasClass('active')) {
                tabItem.removeClass('active').animate({'height': 88}, 200);
                $(this).addClass('active').animate({'height': tabContHeight + 89}, 200);
            } else {
                tabItem.removeClass('active').animate({'height': 88}, 200);
            }
        }

    });

    //Functions

    function tabHeightInit() {

        if ($(window).width() > 960) {
            var tabItemActive = tabs.find('.tab-item.active'),
                linkHeighttabs = tabItemActive.height() + 60,
                tempHeight = tabs.find('.tab-content').height() + linkHeighttabs;
            tabs.find('.tab-item').height('auto');
            tabs.height(tempHeight);
        } else {
            tabs.height('auto');
        }
    }

    function mobileTabInit() {
        var tabItemActive = tabs.find('.tab-item.active'),
            tabContHeight = tabItemActive.find('.tab-content').height();

        tabs.find('.tab-item').height(88);
        tabItemActive.stop().animate({'height': tabContHeight + 89});

    }

});
