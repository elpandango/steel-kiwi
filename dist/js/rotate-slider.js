$(document).ready(function () {

//Rotate slider

    var rotateSlider = $('#rotate-slider'),
        sliderCount = rotateSlider.find('.slides-item').length,
        startSlide = 0,
        currSlide = 0;

    if ($(window).width() > 961) {
        nextSlide();
    }

    //Functions

    function nextSlide() {
        setInterval(function(){
            slideRotate(currSlide);
            currSlide++;
            if (currSlide >= sliderCount) {
                currSlide = startSlide;
            }
        }, 3000);
    }

    function slideRotate(current) {
        var imagesCount = rotateSlider.find('.slides-item').eq(current).find('img').length,
            cur = rotateSlider.find('.slides-item').eq(current);

        rotateSlider.find('.slides-item').eq(current).addClass('active');
        setTimeout(function(){
            imageChange(imagesCount, cur);
            rotateSlider.find('.slides-item').eq(current).removeClass('active');
        }, 1000);
    }

    function imageChange(count, currentItem) {
        var rand = Math.floor((Math.random() * count-1) + 1);
        currentItem.find('img').removeClass('opened');
        currentItem.find('img').eq(rand).addClass('opened');
    }

});