$(document).ready(function () {

//Slider

    $('.js-slider').on('click', '.arrow', function () {
        var number = 0,
            slider = $(this).closest('.js-slider'),
            tempNum = slider.find('img.active').index() + 1,
            sliderWidth = slider.find('.item img').length;

        slider.find('img').animate({'opacity': 0}, 100).removeClass('active');
        number = tempNum;
        if ($(this).hasClass('prev-arrow')) {
            number--;
            if (number < 1) {
                number = sliderWidth;
            }
            changeSlide(slider, number);
        } else {
            number++;
            if (number > sliderWidth) {
                number = 1;
            }
            changeSlide(slider, number);
        }

    });

    //Functions

    function changeSlide(slider, number) {
        slider.find('img').eq(number - 1).addClass('active').animate({'opacity': 1}, 100);
    }

});
